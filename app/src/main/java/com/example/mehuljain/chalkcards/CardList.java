package com.example.mehuljain.chalkcards;

import android.graphics.Bitmap;

public class CardList implements java.io.Serializable  {
    Bitmap image;

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
