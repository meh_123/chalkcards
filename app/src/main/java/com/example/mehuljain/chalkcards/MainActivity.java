package com.example.mehuljain.chalkcards;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{

    RecyclerView cardLayout;
    List<CardList> cardList;
    RecyclerView.Adapter imageHolder;
    ImageButton add_card,add_card_main,preview_card;
    LinearLayoutManager card_manager;
    DisplayMetrics displayMetrics = new DisplayMetrics();
    int REQUEST_CAMERA = 1000;
    int current_card_position,preview_status;
    private int GET_IMAGE_REQUEST = 1;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private Bitmap bitmap;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        preview_status = 0;

        cardList = new ArrayList<>();
        cardLayout = (RecyclerView)findViewById(R.id.cardView);
        add_card_main = (ImageButton)findViewById(R.id.buttonAddMain);
        add_card = (ImageButton)findViewById(R.id.buttonAdd);

        preview_card = (ImageButton)findViewById(R.id.preview);

        cardLayout.setHasFixedSize(true);
        final LinearLayoutManager card_manager = new LinearLayoutManager(getApplicationContext());
        card_manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        cardLayout.setLayoutManager(card_manager);
        imageHolder = new ImageHolder(getApplication(), cardList);
        imageHolder.setHasStableIds(false);
        cardLayout.setAdapter(imageHolder);

        add_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardList.add(cardList.size(),new CardList());
                imageHolder.notifyItemInserted(cardList.size()-1);
                cardLayout.scrollToPosition(cardList.size()-1);
                add_card.setVisibility(View.INVISIBLE);
                add_card_main.setVisibility(View.VISIBLE);
                Log.d("size of cards",cardList.size()+"");
            }
        });
        add_card_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardList.add(cardList.size(),new CardList());
                imageHolder.notifyItemInserted(cardList.size()-1);
                cardLayout.scrollToPosition(cardList.size()-1);
                Log.d("size of cards",cardList.size()+"");
            }
        });
        preview_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cardList.size()<1)
                {
                    Toast.makeText(getApplication(),"Add a Card to Preview",Toast.LENGTH_SHORT).show();
                }
                else if(check_cards_exists(cardList)){

                }
                else {
                    if (preview_status == 0) {
                        cardLayout.scrollToPosition(0);
                        preview_status = 1;
                        preview_card.setImageResource(R.drawable.back_button);
                        add_card_main.setVisibility(View.INVISIBLE);
                        setMargins(cardLayout, 0, 0, 0, 0);
                    } else {
                        preview_status = 0;
                        preview_card.setImageResource(R.drawable.preview);
                        add_card_main.setVisibility(View.VISIBLE);
                        setMargins(cardLayout, 10, 100, 10, 10);
                    }
//                Intent intent = new Intent(MainActivity.this,PreviewCards.class);
//                intent.putExtra("list", (Serializable) cardList);
//                startActivity(intent);
                    // save the task list to preference
//                SharedPreferences prefs = getSharedPreferences("list", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = prefs.edit();
//                try {
//                    editor.putString("imagelist", ObjectSerializer.serialize((Serializable)cardList));
//                    Log.d("dfddfd",prefs.getString("imagelist",ObjectSerializer.serialize(new ArrayList<CardList>())));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                editor.commit();
//                startActivity(intent);
                }
            }
        });


        verifyStoragePermissions(this);
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    public class ImageHolder extends RecyclerView.Adapter<MainActivity.ImageHolder.viewHolder>{
        private Context context;
        public List<CardList> cardList;
        public ImageHolder(Context context , List<CardList> list) {
            this.context = context;
            this.cardList = list;
        }

        @Override
        public MainActivity.ImageHolder.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).
                    inflate(R.layout.view_card, parent, false);

            return new viewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MainActivity.ImageHolder.viewHolder holder, final int position) {

            if (cardList.get(holder.getAdapterPosition()).getImage() != null) {
                holder.cardImage.setVisibility(View.VISIBLE);
                holder.camera.setVisibility(View.INVISIBLE);
                holder.storage.setVisibility(View.INVISIBLE);
                holder.cardImage.setImageBitmap(cardList.get(holder.getAdapterPosition()).getImage());
            } else {
                holder.camera.setVisibility(View.VISIBLE);
                holder.storage.setVisibility(View.VISIBLE);
                holder.cardImage.setVisibility(View.INVISIBLE);
                holder.camera.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get image from camera
                        current_card_position = holder.getAdapterPosition();
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        String dir = Environment.getExternalStorageDirectory().getAbsolutePath();
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory().getAbsoluteFile(), "temp.jpg")));
//                        Log.d(Environment.getExternalStorageDirectory().toString(), Environment.getExternalStorageDirectory().toString());
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }
                });
                Log.d("reached", current_card_position + "");
                holder.storage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get image from storage
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        current_card_position = holder.getAdapterPosition();
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GET_IMAGE_REQUEST);
                    }
                });
                holder.remove_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //remove the Current Card from the list
                        try {
                            current_card_position = holder.getAdapterPosition();
                            Log.d("removed card at ", current_card_position + "");
                            cardList.remove(holder.getAdapterPosition());
                            imageHolder.notifyItemRemoved(holder.getAdapterPosition());
                            imageHolder.notifyItemRangeChanged(holder.getAdapterPosition(), cardList.size());
                            Toast.makeText(getApplication(),"Removed card at Position "+(current_card_position+1),Toast.LENGTH_SHORT).show();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return cardList.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public class viewHolder extends RecyclerView.ViewHolder{
            ImageView cardImage;
            ImageButton storage,camera,remove_card;
            public viewHolder(View itemView) {
                super(itemView);
                // declare variables here
                cardImage = (ImageView)itemView.findViewById(R.id.imageCard);
                storage = (ImageButton)itemView.findViewById(R.id.storage);
                camera = (ImageButton)itemView.findViewById(R.id.camera);
                remove_card = (ImageButton)itemView.findViewById(R.id.removeCard);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_IMAGE_REQUEST && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                CardList object = cardList.get(current_card_position);
                object.setImage(bitmap);
                imageHolder.notifyDataSetChanged();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            try {
                File picture = new File(Environment.getExternalStorageDirectory().getAbsoluteFile() + "/temp.jpg");
                Uri imgUri = Uri.fromFile(picture);
                bitmap = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), imgUri);
                int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                CardList object = cardList.get(current_card_position);
                object.setImage(scaled);
                imageHolder.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
                CardList object = cardList.get(current_card_position);
                object.setImage(null);
                imageHolder.notifyDataSetChanged();
            }
        }
    }
    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 1);
            }
        }
    }

    public List<CardList> getCardList() {
        return cardList;
    }

    public void setCardList(List<CardList> cardList) {
        this.cardList = cardList;
    }
    public boolean check_cards_exists(List<CardList> cardList){
        for(int i=0;i<cardList.size();i++)
        {
            if(cardList.get(i).getImage()== null){
                Toast.makeText(getApplication(),"Add an image to card at position "+(i+1),Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return false;
    }
}
